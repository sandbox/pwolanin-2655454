<?php

/**
 * @file
 * Extends the basic 'node' field handler.
 */

/**
 * Field handler to provide simple renderer that allows linking to a node.
 * Definition terms:
 * - link_to_node default: Should this field have the checkbox "link to node" enabled by default.
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_node_query extends views_handler_field_node {

  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_node_querystring'] = array('default' => isset($this->definition['link_to_node_querystring default']) ? $this->definition['link_to_node_querystring default'] : FALSE, 'bool' => TRUE);
    return $options;
  }

  /**
   * Provide link to node option with query string
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['link_to_node_querystring'] = array(
      '#title' => t('Add current query string to link'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_node_querystring']),
      '#states' => array(
        // Hide the settings when the link to node checkbox is disabled.
        'invisible' => array(
         ':input[name="options[link_to_node]"]' => array('checked' => FALSE),
        ),
      ),
    );
  }

  /**
   * Render whatever the data is as a link to the node.
   *
   * Data should be made XSS safe prior to calling this function.
   *
   * @override
   */
  function render_link($data, $values) {
    if (!empty($this->options['link_to_node']) && !empty($this->additional_fields['nid'])) {
      if ($data !== NULL && $data !== '') {
        $this->options['alter']['make_link'] = TRUE;
        $this->options['alter']['path'] = "node/" . $this->get_value($values, 'nid');
        if (!empty($this->options['link_to_node_querystring'])) {
          $querystring = drupal_http_build_query(drupal_get_query_parameters(NULL, array('q', 'page')));
          if ($querystring) {
            $this->options['alter']['path'] .= '?' . $querystring;
          }
        }
        if (isset($this->aliases['language'])) {
          $languages = language_list();
          $language = $this->get_value($values, 'language');
          if (isset($languages[$language])) {
            $this->options['alter']['language'] = $languages[$language];
          }
          else {
            unset($this->options['alter']['language']);
          }
        }
      }
      else {
        $this->options['alter']['make_link'] = FALSE;
      }
    }
    return $data;
  }
}
